import sys
from subprocess import Popen, PIPE, STDOUT

input_file = open('./'+sys.argv[1])
solution_file = open('./'+sys.argv[2])
exec_command = sys.argv[3]

inputs = input_file.read().split('\n\n')
solutions = solution_file.read().split('\n\n')

for i in range(len(inputs)):
    pipe = Popen(exec_command, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    result = pipe.communicate(input=inputs[i])
    output = result[0]
    error = result[1]

    if len(error) > 0:
        print("Error running test #"+str(i))
        print(output)
        print(error)
    else:
        if output == solutions[i]:
            print("Pass test #"+str(i))
        else:
            print("Failed test #"+str(i))